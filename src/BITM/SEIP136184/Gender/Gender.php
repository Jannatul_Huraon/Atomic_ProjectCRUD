<?php
namespace App\Bitm\SEIP136184\Gender;

use App\BITM\SEIP136184\Utility\Utility;
use App\BITM\SEIP136184\Message\Message;

class Gender
{
    public $id = "";
    public $name="";
    public $gender = "";
    public $conn;
    public $deleted_at;


    public function prepare($data = "")
    {
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists("name", $data)) {
            $this->name= $data['name'];
        }


        if (array_key_exists("gender", $data)) {
            $this->gender = $data['gender'];
        }
        //echo $this->gender;
        //echo $this->name;


    }

    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomic_project") or die("Database connection failed");
    }

    public function store(){
        $query="INSERT INTO `atomic_project`.`gender` (`username`, `gender`) VALUES ('".$this->name."', '".$this->gender."')";

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }

    public function view()
    {
        $query = "SELECT * FROM `gender` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `atomic_project`.`gender` SET `username` = '" . $this->name . "', `gender` = '" . $this->gender . "' WHERE `gender`.`id` = " . $this->id;
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `atomic_project`.`gender` WHERE `gender`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomic_project`.`gender` SET `deleted_at` =" . $this->deleted_at . " WHERE `gender`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been trashed successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been trashed successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allgender = array();
        $query = "SELECT * FROM `gender` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allgender[] = $row;
        }

        return $_allgender;


    }

    public function recover()
    {

        $query = "UPDATE `atomic_project`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomic_project`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomic_project`.`gender` WHERE `gender`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomic_project`.`gender` WHERE `deleted_at` IS NULL ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }



    public function paginator($pageStartFrom=0,$Limit=5){
        $_allGender = array();
        $query="SELECT * FROM `gender` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allGender[] = $row;
        }

        return $_allGender;

    }


}