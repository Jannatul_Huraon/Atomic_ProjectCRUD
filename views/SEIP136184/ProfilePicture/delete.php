<?php
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP136184\ProfilePicture\ImageUploader;


$profile_picture= new ImageUploader();
$single_info=$profile_picture->prepare($_GET)->view();
unlink($_SERVER['DOCUMENT_ROOT'].'/AtomicProject_B22_JannatulHuraon_136184/Resources/Images/'.$single_info->images);
$profile_picture->prepare($_GET)->delete();