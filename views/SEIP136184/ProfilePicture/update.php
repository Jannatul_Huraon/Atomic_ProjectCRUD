<?php
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP136184\ProfilePicture\ImageUploader;
$profile_picture= new ImageUploader();
$single_info=$profile_picture->prepare($_POST)->view();
if((isset($_FILES['image'])&& !empty($_FILES['image']['name']))){
    $image_name= time().$_FILES['image']['name'];
    $temporary_location= $_FILES['image']['tmp_name'];
    unlink($_SERVER['DOCUMENT_ROOT'].'/AtomicProject_B22_JannatulHuraon_136184/Resources/Images/'.$single_info->images);
    move_uploaded_file( $temporary_location,'../../../Resources/Images/'.$image_name);
    $_POST['image']=$image_name;

}



$profile_picture->prepare($_POST)->update();
