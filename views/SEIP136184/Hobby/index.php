<?php

session_start();

include_once ('../../../vendor/autoload.php');

use App\BITM\SEIP136184\Hobby\Hobby;
use App\BITM\SEIP136184\Message\Message;
use App\BITM\SEIP136184\Utility\Utility;

$obj =new Hobby();

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$obj->count();



$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
$allHobby=$obj->paginator($pageStartFrom,$itemPerPage);

//$obj->prepare($_POST);
//$allHobby = $obj->index();

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<div align="right">
    <h3>Back to: </h3>
    <a href="../../../index.php" class="btn btn-success btn-lg" role="button">Atomic Project Index</a>
</div>


<div class="container">
    <center><h2>Subscribers Hobbies</h2></center>
   <!-- <p>
        Contextual classes can be used to color table rows or table cells.
        The classes that can be used are: .active, .success, .info, .warning, and .danger.
    </p> -->

    <a href="create.php" class="btn btn-primary" role="button">Create Again</a>
    <a href="trashlist.php" class="btn btn-primary" role="button">View Trash List</a>

    <div class="alert alert-info" id="message">
        <center><?php
            if(array_key_exists("message",$_SESSION) && (!empty($_SESSION['message'])))
            echo Message::message()
            ?></center>
    </div>
    <form role="form">
        <div class="form-group">
            <label for="sel1">Select how many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option <?php if($itemPerPage==5):?>selected<?php endif ?>>5</option>
                <option <?php if($itemPerPage==10):?>selected<?php endif?>>10</option>
                <option <?php if($itemPerPage==15):?>selected<?php endif ?>>15</option>
                <option <?php if($itemPerPage==20):?>selected<?php endif ?>>20</option>
                <option <?php if($itemPerPage==25):?>selected<?php endif ?>>25</option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>
    <table class="table">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Hobbies</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $count =0 ;
        foreach ($allHobby as $item) { $count++;
            ?>
        <tr class="success">


            <td><?php echo $count?> </td>
            <td> <?php echo $item->id ?> </td>
            <td> <?php echo $item->hobby ?> </td>
            <td>
                <a href="view.php?id=<?php echo $item->id?>" class="btn btn-danger" role="button">View</a>
                <a href="edit.php?id=<?php echo $item->id?>" class="btn btn-info" role="button">Update</a>
                <a href="trash.php?id=<?php echo $item->id?>" class="btn btn-warning" role="button">Trash</a>
                <a href="delete.php?id=<?php echo $item->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
            </td>
        </tr>
       <?php } ?>
        </tbody>
    </table>
</div>
<ul class="pagination">
    <?php if($pageNumber>1):?>
        <li><a href="index.php?pageNumber=<?php echo $pageNumber-1 ?>">Previous</a></li><?php endif;?>
    <?php echo $pagination?>
    <?php if($pageNumber<$totalPage):?>
        <li><a href="index.php?pageNumber=<?php echo $pageNumber+1 ?>">Next</a></li><?php endif;?>
</ul>
<script>
    $('#message').show().delay(2840).fadeOut();


    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>

</body>
</html>