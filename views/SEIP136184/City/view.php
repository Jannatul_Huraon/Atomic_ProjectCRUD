<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP136184\City\City;
use App\BITM\SEIP136184\Utility\Utility;
use App\BITM\SEIP136184\Message\Message;

$info= new City();
$info->prepare($_GET);
$singleItem=$info->view();
//Utility::d($singleItem);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Info</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<a href="create.php" class="btn btn-primary" role="button">Create again</a>  <a href="trashed.php" class="btn btn-primary" role="button">View Trashed list</a> <a href="index.php" class="btn btn-primary" role="button">View List</a>
<div class="container">
    <h2><?php echo $singleItem->username?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleItem->id?></li>
        <li class="list-group-item">User Name: <?php echo $singleItem->username?></li>
        <li class="list-group-item">City: <?php echo $singleItem->city?></li>
    </ul>
</div>

</body>
</html>