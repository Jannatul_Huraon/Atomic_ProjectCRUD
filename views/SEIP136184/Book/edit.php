<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP136184\Book\Book;
use App\Bitm\SEIP136184\Utility\Utility;

$book= new Book();
$book->prepare($_GET);
$singleItem=$book->view();
//Utility::d($singleItem);
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <title>CRUD-BOOK</title>

</head>
<body>

<div class="container">
    <h2>Edit Book Title</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit Book Title:</label>
            <input type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
            <input type="text" name="title" class="form-control" id="title" placeholder="Enter Book Title" value="<?php echo $singleItem->title?>">
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
