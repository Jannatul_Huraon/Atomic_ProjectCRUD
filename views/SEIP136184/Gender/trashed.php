<?php

include_once('../../../vendor/autoload.php');
use App\BITM\SEIP136184\Gender\Gender;
use App\BITM\SEIP136184\Utility\Utility;
use App\BITM\SEIP136184\Message\Message;

$gender= new Gender();
$allGender=$gender->trashed();



?>

<!DOCTYPE html>
<html>
<head>

</head>
<body>

<div class="container">
    <h2>Trashed List</h2>
    <form action="recovermultiple.php" method="post" id="multiple">
        <a href="index.php" class="btn btn-primary" role="button">See All List</a>
        <button type="submit"  class="btn btn-primary">Recover Selected</button>
        <button type="button"  class="btn btn-primary" id="multiple_delete">Delete Selected</button>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Check Item</th>
                    <th>#</th>
                    <th>ID</th>
                    <th>User Name</th>
                    <th>Gender</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                    $sl=0;
                    foreach($allGender as $gender){
                    $sl++; ?>
                    <td><input type="checkbox" name="mark[]" value="<?php echo $gender->id?>"></td>
                    <td><?php echo $sl?></td>
                    <td><?php echo $gender-> id?></td>
                    <td><?php echo $gender->username?></td>
                    <td><?php echo $gender->gender?></td>
                    <td><a href="recover.php?id=<?php echo $gender-> id ?>" class="btn btn-primary" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $gender->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                    </td>

                </tr>
                <?php }?>


                </tbody>
            </table>
    </form>
</div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    $(document).ready(function(){
        $("#delete").click(function(){
            if (!confirm("Do you want to delete")){
                return false;
            }
        });
    });

    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>

</body>
</html>

